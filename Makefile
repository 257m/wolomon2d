STACK_SIZE=20971520
HEAP_SIZE=67108864

all:
	odin build src

wasm:
	mkdir -p build
	odin build src -target=freestanding_wasm32 -out:odin -build-mode:obj -debug -show-system-calls
	emcc -o index.html src/main.c odin.wasm.o lib/libraylib.a lib/libraygui.a -s ASYNCIFY -s FORCE_FILESYSTEM=1 -s EXPORTED_RUNTIME_METHODS='["FS"]' --preload-file assets -s USE_GLFW=3  -s GL_ENABLE_GET_PROC_ADDRESS -DWEB_BUILD -sSTACK_SIZE=$(STACK_SIZE) -s TOTAL_MEMORY=$(HEAP_SIZE) -sERROR_ON_UNDEFINED_SYMBOLS=0 -fsanitize=undefined -s SAFE_HEAP -s ASSERTIONS=1 -s ALLOW_MEMORY_GROWTH=1
