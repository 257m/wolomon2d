package main

import rl "raylib"
import "core:time"
import "core:fmt"
import glm "core:math/linalg/glsl"
import rand "core:math/rand"
import math "core:math"

ai_funcs := #partial [EntityType]proc(e: ^Entity){
	.TRAINER = player_controls,
	.WOLOMON = wolomon_ai,
	.PROJECTILE = projectile_ai,
}

trainer_ai :: proc(e: ^Entity)
{
	if rl.IsKeyPressed(.W) || rl.IsKeyPressed(.UP) {
		e.dir = .UP
		e.anim = .WALKING
		e.anim_start = time.tick_now()
	}
	if rl.IsKeyPressed(.A) || rl.IsKeyPressed(.LEFT) {
		e.dir = .LEFT
		e.anim = .WALKING
		e.anim_start = time.tick_now()
	}
	if rl.IsKeyPressed(.S) || rl.IsKeyPressed(.DOWN) {
		e.dir = .DOWN
		e.anim = .WALKING
		e.anim_start = time.tick_now()
	}
	if rl.IsKeyPressed(.D) || rl.IsKeyPressed(.RIGHT) {
		e.dir = .RIGHT
		e.anim = .WALKING
		e.anim_start = time.tick_now()
	}
	
	if e.anim != .ATTACK {
		if rl.IsKeyPressed(.H) {
			e.dir = .LEFT
			e.anim = .ATTACK
			e.anim_start = time.tick_now()
			e.vel /= 16
			em_add(&em, Entity{p = {e = {glm.vec2{e.x-50, e.y}, 10, {0, 0}, {0, 0}, {21, 16}, .FIREBALL, .LEFT, .FIREBALL, time.tick_now(), .PROJECTILE, 0, false}, move = e.t.moves[e.t.current_move].move, origin = em.ids[e.tag]}})
		}
		if rl.IsKeyPressed(.J) {
			e.dir = .DOWN
			e.anim = .ATTACK
			e.anim_start = time.tick_now()
			e.vel /= 16
			em_add(&em, Entity{p = {e = {glm.vec2{e.x, e.y+50}, 10, {0, 0}, {0, 0}, {16, 21}, .FIREBALL, .DOWN, .FIREBALL, time.tick_now(), .PROJECTILE, 0, false}, move = e.t.moves[e.t.current_move].move, origin = em.ids[e.tag]}})
		}
		if rl.IsKeyPressed(.K) {
			e.dir = .UP
			e.anim = .ATTACK
			e.anim_start = time.tick_now()
			e.vel /= 16
			em_add(&em, Entity{p = {e = {glm.vec2{e.x, e.y-50}, 10, {0, 0}, {0, 0}, {16, 21}, .FIREBALL, .UP, .FIREBALL, time.tick_now(), .PROJECTILE, 0, false}, move = e.t.moves[e.t.current_move].move, origin = em.ids[e.tag]}})
		}
		if rl.IsKeyPressed(.L) {
			e.dir = .RIGHT
			e.anim = .ATTACK
			e.anim_start = time.tick_now()
			e.vel /= 16
			em_add(&em, Entity{p = {e = {glm.vec2{e.x+50, e.y}, 10, {0, 0}, {0, 0}, {21, 16}, .FIREBALL, .RIGHT, .FIREBALL, time.tick_now(), .PROJECTILE, 0, false}, move = e.t.moves[e.t.current_move].move, origin = em.ids[e.tag]}})
		}
	}

	movespeed := f32(MOVESPEED)*4
	if (rl.IsKeyDown(.A) || rl.IsKeyDown(.LEFT) || rl.IsKeyDown(.D) || rl.IsKeyDown(.RIGHT)) &&
	(rl.IsKeyDown(.W) || rl.IsKeyDown(.UP) || rl.IsKeyDown(.S) || rl.IsKeyDown(.DOWN)) {
		movespeed *= 0.707106781187
	}
	if rl.IsKeyDown(.W) || rl.IsKeyDown(.UP) {
		e.vel.y += (-movespeed-e.vel.y)*game.dt
		if (e.anim == .WALKING) {
			e.dir = .UP
		}
	}
	if rl.IsKeyDown(.A) || rl.IsKeyDown(.LEFT) {
		e.vel.x += (-movespeed-e.vel.x)*game.dt
		if (e.anim == .WALKING) {
			e.dir = .LEFT
		}
	}
	if rl.IsKeyDown(.S) || rl.IsKeyDown(.DOWN) {
		e.vel.y += (movespeed-e.vel.y)*game.dt
		if (e.anim == .WALKING) {
			e.dir = .DOWN
		}
	}
	if rl.IsKeyDown(.D) || rl.IsKeyDown(.RIGHT) {
		e.vel.x += (movespeed-e.vel.x)*game.dt
		if (e.anim == .WALKING) {
			e.dir = .RIGHT
		}
	}

	if rl.IsKeyUp(.W) && rl.IsKeyUp(.A) && rl.IsKeyUp(.S) && rl.IsKeyUp(.D) &&
	rl.IsKeyUp(.UP) && rl.IsKeyUp(.LEFT) && rl.IsKeyUp(.DOWN) && rl.IsKeyUp(.RIGHT) && e.anim == .WALKING {
		e.anim = .IDLE
	}
}

player_controls :: proc(e: ^Entity) {
	if rl.IsKeyDown(.Z) {
		if rl.IsKeyPressed(.ONE)   do e.t.current_control = 0
		if rl.IsKeyPressed(.TWO)   do e.t.current_control = 1
		if rl.IsKeyPressed(.THREE) do e.t.current_control = 2
		if rl.IsKeyPressed(.FOUR)  do e.t.current_control = 3
		if rl.IsKeyPressed(.ZERO)  do e.t.current_control = nil
	}
	else {
		if rl.IsKeyPressed(.ONE)   do e.t.current_move = 0
		if rl.IsKeyPressed(.TWO)   do e.t.current_move = 1
		if rl.IsKeyPressed(.THREE) do e.t.current_move = 2
		if rl.IsKeyPressed(.FOUR)  do e.t.current_move = 3
	}


	if _, ok := e.t.current_control.?; ok do return
	
	trainer_ai(e)
}

wolomon_ai :: proc(e: ^Entity)
{
	w := &e.w
	p: ^Entity
	if owner, ok := w.owner.?; ok {
		p = &em.e[owner]
		if control, ok := p.t.current_control.?; ok {
			if p.t.mon[control] == em.ids[e.tag] do trainer_ai(e)
			return
		}
	}
	else {
		start := glm.ivec2{i32(math.floor((e.pos.x - 8*e.hitbox.x) / GRID_DIM)), i32(math.floor((e.pos.y - 8*e.hitbox.y) / GRID_DIM))}
		end := glm.ivec2{i32(math.ceil((e.pos.x + 8*e.hitbox.x) / GRID_DIM)), i32(math.ceil((e.pos.y + 8*e.hitbox.y) / GRID_DIM))}
		best_dist := f32(0)
		for x in start.x..=end.x {
			for y in start.y..=end.y {
				m, ok := collision_map[glm.ivec2{x, y}]
				if !ok do continue
				// Reverse to deal with deleting elements moving the array over
				#reverse for id, i in m {
					if id == em.ids[e.tag] do continue
					if em.e[id].type == .WOLOMON || em.e[id].type == .TRAINER {
						if p != nil {
							dist := abs(w.x - em.e[id].x) + abs(w.y - em.e[id].y)
							if dist < best_dist {
								p = &em.e[id]
								best_dist = dist
							}
						}
						else { 
							p = &em.e[id]
							best_dist = abs(w.x - p.x) + abs(w.y - p.y)
						}
					}
				}
			}
		}
	}
	if p != nil {
		if w.y < p.y {
			w.vel.y += (MOVESPEED/2)*game.dt
		}
		else if w.y > p.y {
			w.vel.y -= (MOVESPEED/2)*game.dt
		}
		if w.x < p.x {
			w.vel.x += (MOVESPEED/2)*game.dt
		}
		else if w.x > p.x {
			w.vel.x -= (MOVESPEED/2)*game.dt
		}
		w.dir = get_diff_direction(w.pos, p.pos)
	}
	if rand.float32() < 0.01 {
		#partial switch w.dir {
			case .LEFT:
			em_add(&em, Entity{p = {e = {glm.vec2{w.x-50, w.y}, 10, {0, 0}, {0, 0}, {21, 16}, .FIREBALL, .LEFT, .FIREBALL, time.tick_now(), .PROJECTILE, 0, false}, move = w.moves[0].move, origin = em.ids[w.tag]}})
			case .UP:
			em_add(&em, Entity{p = {e = {glm.vec2{w.x, w.y-50}, 10, {0, 0}, {0, 0}, {16, 21}, .FIREBALL, .UP, .FIREBALL, time.tick_now(), .PROJECTILE, 0, false}, move = w.moves[0].move, origin = em.ids[w.tag]}})
			case .RIGHT:
			em_add(&em, Entity{p = {e = {glm.vec2{w.x+50, w.y}, 10, {0, 0}, {0, 0}, {21, 16}, .FIREBALL, .RIGHT, .FIREBALL, time.tick_now(), .PROJECTILE, 0, false}, move = w.moves[0].move, origin = em.ids[w.tag]}})
			case .DOWN:
			em_add(&em, Entity{p = {e = {glm.vec2{w.x, w.y+50}, 10, {0, 0}, {0, 0}, {16, 21}, .FIREBALL, .DOWN, .FIREBALL, time.tick_now(), .PROJECTILE, 0, false}, move = w.moves[0].move, origin = em.ids[w.tag]}})
		}
	}
}

projectile_ai :: proc(p: ^Entity)
{
	#partial switch p.dir {
		case .DOWN: p.vel = {0, 400}
		case .UP: p.vel = {0, -400}
		case .RIGHT: p.vel = {400, 0}
		case .LEFT: p.vel = {-400, 0}
	}
}

entity_ai :: proc(e: ^Entity)
{
	ai_funcs[e.type](e)
}
