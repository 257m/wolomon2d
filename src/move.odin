package main

MoveFlags :: enum {
}

Category ::enum {
	STATUS,
	PHYSICAL,
	MAGICAL,
}

Boost :: struct {
	boost: i32,
	chance: f32,
}

MoveData :: struct {
	name: string,
	type: Type,
	power: u32,
	category: Category,
	accuracy: f32,
	usages: u32,
	flags: bit_set[MoveFlags],

	selfBoost: Maybe(#sparse [Stat]Boost),
	otherBoost: Maybe(#sparse [Stat]Boost),

	movefunc: proc(^Entity, ^Entity),
}

MoveSlot :: struct {
	move: MoveIndex,
	usages: u32,
	max_usage: u32,
	disabled, used: bool,
}
