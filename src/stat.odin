package main

BaseStat :: enum {
	HP,
	ATK,
	DEF,
	MGA,
	MGD,
	SPE,
}

Stat :: enum {
	ATK,
	DEF,
	MGA,
	MGD,
	SPE,
	ACC,
	EVA,
	CRT,
}

stat_names := []string {
	"Attack",
	"Defense",
	"Magical Attack",
	"Magical Defense",
	"Speed",
	"Accuracy",
	"Evasion",
	"Crit Ratio",
}

get_stat_of_set :: proc(w: ^WolomonSet, stat: BaseStat) -> u32 {
	if (stat == .HP) {
		return (w.genes[stat] + 2 * Species[w.specie].baseStats[stat] + u32(f32(w.skill[stat])/4 * f32(w.level)/100)) + 10 + w.level
	}
	return (w.genes[stat] + 2 * Species[w.specie].baseStats[stat] + u32(f32(w.skill[stat])/4 * f32(w.level)/100) ) + 5

}

get_stat_of_wolomon :: proc(w: ^Wolomon, stat: Stat) -> u32
{
	// Stat before boost
	if stat <= .SPE { 
		base := get_stat_of_set(&w.set, BaseStat(int(stat)+1))

		// Return base * statBoost
		return u32(f64(base) * (w.statBoosts[stat] < 0 ? 2/2-f64(w.statBoosts[stat]) : 1 + f64(w.statBoosts[stat])/2))
	}
	return 0
}

get_stat :: proc{
	get_stat_of_set,
	get_stat_of_wolomon,
}
