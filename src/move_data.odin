package main

MoveIndex :: enum {
	NULL,
	CAPTURE,
	BITE,
	FLAMESHOT,
}

capture :: proc(e: ^Entity, o: ^Entity)
{
	t := &em.e[e.p.origin].t
	w := &o.w
	for m, i in t.mon do if mon, ok := m.?; !ok {
		t.mon[i] = em.ids[o.tag]
		w.owner = e.p.origin
		break;
	}
}

Moves := [MoveIndex]MoveData{
	.NULL = {
		name = "Null",
		type = .NULL,
		power = 0,
	},
	.CAPTURE = {
		name = "Capture",
		type = .NULL,
		power = 0,
		category = .STATUS,
		movefunc = capture,
	},
	.BITE = {
		name = "Bite",
		type = .NORMAL,
		power = 60,
		category = .PHYSICAL,
		accuracy = 0.95,
	},
	.FLAMESHOT = {
		name = "Flameshot",
		type = .FIRE,
		power = 80,
		category = .MAGICAL,
	},
}
