package main
import "base:runtime"
import "core:mem"
import "core:c"
import rl "raylib"

IS_WASM :: ODIN_ARCH == .wasm32 || ODIN_ARCH == .wasm64p32

when IS_WASM {
	foreign import "odin_env"

	@(export, link_name="_main")
	_main :: proc "c" () {
		using runtime
		context = default_context()

		context.allocator      = rl.MemAllocator()

		temp_arena: Arena
		err := arena_init(&temp_arena, 2097152, context.allocator)
		if err != nil {
			panic("Oh no")
		}
		context.temp_allocator = arena_allocator(&temp_arena)

		entry()
	}
} else {
	main :: proc () {
		entry()
	}
}
