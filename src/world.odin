package main

import "core:fmt"
import "core:math"
import noise "core:math/noise"
import glm "core:math/linalg/glsl"
import rl "raylib"

Tile :: enum {
	GRASS,
	SEEDLING,
	WATER,
}

WORLD_WIDTH :: 22
WORLD_HEIGHT :: 14

world: map[glm.ivec2]Chunk
world_seed: i64

world_init :: proc(seed: i64) {
	world = make(map[glm.ivec2]Chunk)
	world_seed = seed
}

world_get :: proc(coord: glm.ivec2) -> Tile
{
	chunk_coord := glm.ivec2{i32(math.floor(f32(coord.x)/CHUNK_WIDTH)), i32(math.floor(f32(coord.y)/CHUNK_HEIGHT))}
	existing_chunk, exists := world[chunk_coord]
	if exists do return existing_chunk.tiles[coord.y - chunk_coord.y*CHUNK_HEIGHT][coord.x - chunk_coord.x*CHUNK_WIDTH]
	chunk := chunk_load(chunk_coord)
	return chunk.tiles[coord.y - chunk_coord.y*CHUNK_HEIGHT][coord.x - chunk_coord.x*CHUNK_WIDTH]
}

world_render :: proc()
{
	for i in 0..<WORLD_HEIGHT {
		for j in 0..<WORLD_WIDTH {
			tile := i32(world_get(glm.ivec2{i32(j-WORLD_WIDTH/2)+i32(em.e[player].pos.x/64), i32(i-WORLD_HEIGHT/2)+i32(em.e[player].pos.y/64)}))
			rl.DrawTexturePro(textures[.OVERWORLD], rl.Rectangle{f32(16*(tile % 40)), f32(16*(tile / 40)), 16, 16}, rl.Rectangle{f32((j-1)*64)-f32(em.e[player].pos.x)+f32(i32(em.e[player].pos.x/64)*64), f32((i-1)*64)-f32(em.e[player].pos.y)+f32(i32(em.e[player].pos.y/64)*64), 64, 64}, rl.Vector2{0, 0}, 0, rl.WHITE)
		}
	}
}
