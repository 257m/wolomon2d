package main
import glm "core:math/linalg/glsl"
import "core:os"
import "core:strings"
import "core:slice"

CHUNK_WIDTH :: 16
CHUNK_HEIGHT :: 16
Chunk :: struct {
	tiles: [CHUNK_HEIGHT][CHUNK_WIDTH]Tile,
	is_changed: bool
}

chunk_load :: proc(chunk_coord: glm.ivec2) -> ^Chunk {
	if chunk_coord in world {
		return &world[chunk_coord]
	}
	strings.write_string(&sb, "save/chunks/")
	strings.write_int(&sb, int(chunk_coord.x));
	strings.write_byte(&sb, '-')
	strings.write_int(&sb, int(chunk_coord.y));
	fd, err := os.open(strings.to_string(sb), os.O_RDONLY, 0)
	strings.builder_reset(&sb)
	world[chunk_coord] = Chunk{}
	chunk := &world[chunk_coord]
	if err == 0 {
		os.read_full(fd, slice.to_bytes(chunk.tiles[:]))
		os.close(fd)
		chunk.is_changed = false
	}
	else {
		for y in 0..<CHUNK_HEIGHT {
			for x in 0..<CHUNK_WIDTH {
				chunk.tiles[y][x] = Tile.GRASS
			}
		}
		chunk.is_changed = true
	}
	return chunk
}
