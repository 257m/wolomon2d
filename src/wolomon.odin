package main

WolomonSet :: struct {
	specie: SpecieIndex,
	name: string,
	level: u32,
	ability: AbilityIndex,
	moves: [4]MoveSlot,
	genes: [BaseStat]u32,
	skill: [BaseStat]u32,
	gender: Gender,
}

Wolomon :: struct {
	using e: EntityBase,
	using set: WolomonSet,
	statBoosts: [Stat]i32,
	owner: Maybe(uint),
}
