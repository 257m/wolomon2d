package main

import "core:fmt"
import "core:time"
import math "core:math"
import glm "core:math/linalg/glsl"
import rl "raylib"
import rand "core:math/rand"
import "core:strings"

Direction :: enum {
	LEFT,
	UP,
	RIGHT,
	DOWN,
	LOWER_LEFT,
	LOWER_RIGHT,
	UPPER_RIGHT,
	UPPER_LEFT,
}

get_diff_direction :: proc(v: glm.vec2, w: glm.vec2) -> Direction
{
	/*if v.x < w.x && v.y < w.y do return .UPPER_LEFT
	if v.x > w.x && v.y < w.y do return .UPPER_RIGHT
	if v.x < w.x && v.y > w.y do return .LOWER_LEFT
	if v.x > w.x && v.y > w.y do return .UPPER_RIGHT*/

	if abs(v.y - w.y) > abs(v.x - w.x) {
		if v.y > w.y do return .UP
		if v.y < w.y do return .DOWN
	}
	if v.x > w.x do return .LEFT
	return .RIGHT
}

Animation :: enum {
	IDLE,
	WALKING,
	ATTACK,
	FIREBALL,
}

Animation_Data :: struct {
	frames: []glm.ivec2,
	frame_size: glm.ivec2,
	interval: u64,
	is_looped: bool,
	is_rotated: bool,
}

animations: [Animation]Animation_Data = {
	.IDLE = {
		frames = {{0, 0}, {32, 0}},
		frame_size = {16, 24},
		interval = 400000000,
		is_looped = true,
	},
	.WALKING = {
		frames = {{0, 0}, {16, 0}, {32, 0}, {48, 0}},
		frame_size = {16, 24},
		interval = 200000000,
		is_looped = true,
	},
	.ATTACK = {
		frames = {{0, 104}, {24, 104}, {48, 104}, {72, 104}},
		frame_size = {24, 26},
		interval = 200000000,
		is_looped = false,
	},
	.FIREBALL = {
		frames = {{0, 0}, {21, 0}, {42, 0}, {63, 0}},
		interval = 50000000,
		frame_size = {21, 16},
		is_looped = true,
		is_rotated = true,
	},
}

EntityType :: enum {
	ENTITY,
	WOLOMON,
	TRAINER,
	PROJECTILE,
}

EntityBase :: struct {
	using pos: glm.vec2,
	hp: int,
	vel: glm.vec2,
	acc: glm.vec2,
	hitbox: glm.vec2,
	texture: Texture,
	dir: Direction,
	anim: Animation,
	anim_start: time.Tick,
	type: EntityType,
	tag: uint,
	dead: bool,
}

Entity :: struct #raw_union { using b: EntityBase, w: Wolomon, t: Trainer, p: Projectile }

entity_collision_update :: proc(e, o: ^Entity) {
	if rl.CheckCollisionRecs(rl.Rectangle{e.pos.x, e.pos.y, e.hitbox.x, e.hitbox.y}, rl.Rectangle{o.pos.x, o.pos.y, o.hitbox.x, o.hitbox.y}) {
		e.vel, o.vel = o.vel, e.vel
		e.vel += (e.pos - o.pos)
		o.vel += (o.pos - e.pos)
	}
}

hitbox_for_each :: proc(e: ^rl.Rectangle, fn: proc(e: ^Entity))
{
	start := glm.ivec2{i32(math.floor((e.x - 2*e.width) / GRID_DIM)), i32(math.floor((e.y - 2*e.height) / GRID_DIM))}
	end := glm.ivec2{i32(math.ceil((e.x + 2*e.width) / GRID_DIM)), i32(math.ceil((e.y + 2*e.height) / GRID_DIM))}
	for x in start.x..=end.x {
		for y in start.y..=end.y {
			m, ok := collision_map[glm.ivec2{x, y}]
			if !ok do continue
			for id, i in m {
				if rl.CheckCollisionRecs(e^, rl.Rectangle{em.e[id].pos.x, em.e[id].pos.y, em.e[id].hitbox.x, em.e[id].hitbox.y}) {
					fn(&em.e[id])
				}
			}
		}
	}
}

handle_move :: proc(a, t: ^Wolomon, m: MoveIndex)
{
	using move_data := Moves[m]
	atk := get_stat(a, Stat(u32(Stat.ATK) + u32(category == .MAGICAL)*2))
	def := get_stat(t, Stat(u32(Stat.DEF) + u32(category == .MAGICAL)*2))
	type_mult := type_chart[move_data.type][Species[t.specie].types[0]] * type_chart[move_data.type][Species[t.specie].types[1]]
	dmg := u32((((f32(2 * a.level / 5 + 2) * f32(atk) * f32(power) / f32(def)) / 50) + 2) * f32(rand.int_max(16)+85)/100)
	t.hp -= auto_cast dmg
}

entity_update :: proc(e: ^Entity)
{
	dist := abs(e.x - em.e[player].x) + abs(e.y - em.e[player].y)
	if dist > 8192 {
		e.dead = true
		return
	}
	e.acc = -4*e.vel
	e.vel += e.acc*game.dt

	start := glm.ivec2{i32(math.floor((e.pos.x - 2*e.hitbox.x) / GRID_DIM)), i32(math.floor((e.pos.y - 2*e.hitbox.y) / GRID_DIM))}
	end := glm.ivec2{i32(math.ceil((e.pos.x + 2*e.hitbox.x) / GRID_DIM)), i32(math.ceil((e.pos.y + 2*e.hitbox.y) / GRID_DIM))}
	for x in start.x..=end.x {
		for y in start.y..=end.y {
			m, ok := collision_map[glm.ivec2{x, y}]
			if !ok do continue
			// Reverse to deal with deleting elements moving the array over
			#reverse for id, i in m {
				if id == em.ids[e.tag] do continue
				if rl.CheckCollisionRecs(rl.Rectangle{e.pos.x, e.pos.y, e.hitbox.x, e.hitbox.y}, rl.Rectangle{em.e[id].pos.x, em.e[id].pos.y, em.e[id].hitbox.x, em.e[id].hitbox.y}) {
					e.vel, em.e[id].vel = em.e[id].vel, e.vel
					e.vel += (e.pos - em.e[id].pos)
					em.e[id].vel += (em.e[id].pos - e.pos)
					if e.type == .WOLOMON || e.type == .TRAINER {
						if em.e[id].type == .PROJECTILE {
							p := &em.e[id].p
							handle_move(&em.e[p.origin].w, &e.w, p.move)
							if Moves[p.move].movefunc != nil do Moves[p.move].movefunc(&em.e[id], e)
							em.e[id].hp = 0
						}
					}
					else if e.type == .PROJECTILE {
						p := &e.p
						if em.e[id].type == .WOLOMON || em.e[id].type == .TRAINER {
							handle_move(&em.e[p.origin].w, &em.e[id].w, p.move)
							if Moves[p.move].movefunc != nil do Moves[p.move].movefunc(e, &em.e[id])
						}
						e.hp = 0
					}
					if em.e[id].hp <= 0 {
						em.e[id].dead = true
						ordered_remove(&m, i)
					}
				}
			}
		}
	}
	if e.hp <= 0 {
		e.dead = true
		return
	}
	coord := glm.ivec2{i32(math.floor(e.pos.x / GRID_DIM)), i32(math.floor(e.pos.y / GRID_DIM))}
	if !(coord in collision_map) do collision_map[coord] = make([dynamic]uint, allocator = context.temp_allocator)
	append(&collision_map[coord], em.ids[e.tag])
}

entity_post_update :: proc(e: ^Entity) {
	if e.dead do em_delete(&em, em.ids[e.tag]) 
	else do e.pos += e.vel*game.dt
}

entity_render_bar :: proc(e: ^Entity, pos: glm.ivec2) {
	scaled_hp := i32(100*(f32(e.hp)/f32(get_stat_of_set(&e.w.set, .HP))))
	rl.DrawRectangle(pos.x, pos.y, scaled_hp, 10, rl.GREEN)
	rl.DrawRectangle(pos.x+scaled_hp, pos.y, 100-scaled_hp, 10, rl.RED)
	rl.DrawRectangleLines(pos.x, pos.y, 100, 10, rl.BLACK)
	name := strings.clone_to_cstring(Species[e.w.specie].name, context.temp_allocator)
	rl.DrawText(name, pos.x + i32(50-rl.MeasureText(name, 10)/2), pos.y, 10, rl.BLACK)
	strings.write_int(&sb, e.hp);
	rl.DrawText(strings.clone_to_cstring(strings.to_string(sb), context.temp_allocator), pos.x + 2, pos.y, 10, rl.BLACK)
	strings.builder_reset(&sb)
}

entity_render :: proc(e: ^Entity)
{
	// Get frame
	frame_number := int((u64(time.tick_since(e.anim_start)) / animations[e.anim].interval)) % len(animations[e.anim].frames)
	using anim := animations[e.anim]
	if abs(e.x - em.e[player].x) > WINDOW_WIDTH/2+f32(4*frame_size.x) do return
	if abs(e.y - em.e[player].y) > WINDOW_HEIGHT/2+f32(4*frame_size.y) do return
	frame := anim.frames[frame_number]
	if frame_number == len(frames)-1 && !anim.is_looped {
		e.anim = .IDLE
		e.anim_start = time.tick_now()
	}

	// Draw sprite
	if anim.is_rotated {
		rl.DrawTexturePro(textures[e.texture],
		rl.Rectangle{f32(frame.x), f32(frame.y), f32(frame_size.x), f32(frame_size.y)},
		rl.Rectangle{f32(WINDOW_WIDTH+(e.hitbox.x))/2+(e.x-em.e[player].x), f32(WINDOW_HEIGHT+(e.hitbox.y))/2+(e.y-em.e[player].y), f32(frame_size.x), f32(frame_size.y)}, rl.Vector2{f32(frame_size.x)/2, f32(frame_size.y)/2}, f32(e.dir)*90, rl.WHITE)
	}
	else {
		rl.DrawTexturePro(textures[e.texture],
		rl.Rectangle{f32(frame.x), f32(frame.y + (frame_size.y)*i32(e.dir)), f32(frame_size.x), f32(frame_size.y)},
		rl.Rectangle{f32(WINDOW_WIDTH-(4*f32(frame_size.x)-e.hitbox.x))/2+(e.x-em.e[player].x), f32(WINDOW_HEIGHT-(4*f32(frame_size.y)-e.hitbox.y))/2+(e.y-em.e[player].y) - f32(4)*4, f32(4*frame_size.x), f32(4*frame_size.y)}, rl.Vector2{0, 0}, 0, rl.WHITE)
	}

	// Draw hitbox
	rl.DrawRectangleLines(i32(WINDOW_WIDTH/2+e.x-em.e[player].x), i32(WINDOW_HEIGHT/2+e.y-em.e[player].y), i32(e.hitbox.x), i32(e.hitbox.y), rl.PINK)
	
	// Draw HP bar
	if e.type == .WOLOMON || e.type == .TRAINER {
		entity_render_bar(e, glm.ivec2{i32(WINDOW_WIDTH-100+e.hitbox.x)/2+i32(e.x-em.e[player].x), i32(WINDOW_HEIGHT-10)/2+i32(e.y-em.e[player].y - 60)})
	}
}

entity_is_colliding :: proc(e1: ^Entity, e2: ^Entity)
{
	if rl.CheckCollisionRecs(rl.Rectangle{e1.pos.x, e1.pos.y, e1.hitbox.x, e1.hitbox.y}, rl.Rectangle{e2.pos.x, e2.pos.y, e2.hitbox.x, e2.hitbox.y}) {
		rec := rl.GetCollisionRec(rl.Rectangle{e1.pos.x, e1.pos.y, e1.hitbox.x, e1.hitbox.y}, rl.Rectangle{e2.pos.x, e2.pos.y, e2.hitbox.x, e2.hitbox.y});
		rl.DrawRectangleRec(rl.Rectangle{(WINDOW_WIDTH-e1.hitbox.x)/2+(rec.x-em.e[player].x), (WINDOW_HEIGHT-e1.hitbox.y)/2+(rec.y-em.e[player].y), rec.width, rec.height}, rl.BLACK)
	}
}
