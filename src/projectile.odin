package main

Projectile :: struct {
	using e: EntityBase,
	move: MoveIndex,
	origin: uint,
}
