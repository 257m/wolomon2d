package main

Gender :: enum {
	GENDERLESS,
	FEMALE,
	MALE,
}

Specie :: struct {
	dexnum: u32,
	name: string,
	types: [2]Type,
	baseStats: [BaseStat]u32,
	abilities: [AbilityType]AbilityIndex,
	evos: []SpecieIndex,
	prevo: SpecieIndex,
	evoLevel: u32,
	height: f32, // meters
	weight: f32, // kg
	genderRatio: f32,
}

SpecieIndex :: enum {
	NULL,
	AXQUAL,
}

Species := [SpecieIndex]Specie{
	.NULL = {},
	.AXQUAL = {
		dexnum = 1,
		name = "Axqual",
		types = { .WATER, .NULL },
		baseStats = { .HP = 45, .ATK = 45, .DEF = 45, .MGA = 60, .MGD = 60, .SPE = 45 },
		evos = {},
		evoLevel = 16,
		height = 1,
		weight = 1,
		genderRatio = 0.5,
	},
}
