package main

Trainer :: struct {
	using w: Wolomon,
	mon: [6]Maybe(uint),
	current_move: u32,
	current_control: Maybe(uint)
}
