package main

import "core:time"
import "core:fmt"
import "core:slice"
import "core:strings"
import rand "core:math/rand"
import "core:thread"
import glm "core:math/linalg/glsl"

DIM :: 16

WFC_Direction :: enum {
	UP,
	RIGHT,
	DOWN,
	LEFT,
}

Cell :: struct {
	superposition: bit_set[Tile],
	pos: [2]i32
}

lookup_table := #sparse [Tile][WFC_Direction]bit_set[Tile]{
	.GRASS = {
		.UP = { .GRASS, .SEEDLING, .WATER },
		.RIGHT = { .GRASS, .SEEDLING, .WATER },
		.DOWN = { .GRASS, .SEEDLING, .WATER },
		.LEFT = { .GRASS, .SEEDLING, .WATER },
	},
	.SEEDLING = {
		.UP = { .GRASS, .SEEDLING },
		.RIGHT = { .GRASS, .SEEDLING },
		.DOWN = { .GRASS, .SEEDLING },
		.LEFT = { .GRASS, .SEEDLING },
	},
	.WATER = {
		.UP = { .GRASS, .WATER, },
		.RIGHT = { .GRASS, .WATER, },
		.DOWN = { .GRASS, .WATER },
		.LEFT = { .GRASS, .WATER },
	},
}

WFC :: struct {
	grid: [(CHUNK_HEIGHT+2)*(CHUNK_WIDTH+2)]Cell,
	gridCopyArray: [(CHUNK_HEIGHT+2)*(CHUNK_WIDTH+2)]^Cell,
	tiles: []^Cell,
}

chunk_generate :: proc(chunk_coord: glm.ivec2) -> ^Chunk {
	world[chunk_coord] = Chunk{}
	chunk := &world[chunk_coord]
	using wfc: WFC
	tiles = gridCopyArray[:]
	for j in 0..<i32(CHUNK_HEIGHT+2) {
		for i in 0..<i32(CHUNK_WIDTH+2) {
			grid[j*(CHUNK_WIDTH+2)+i] = Cell{ superposition = { .GRASS, .SEEDLING, .WATER }, pos = {i, j}}
			tiles[j*(CHUNK_WIDTH+2)+i] = &grid[j*(CHUNK_WIDTH+2)+i]
		}
	}

	using time
	for {
		s: Stopwatch
		stopwatch_start(&s)
		if wfc_update(&wfc, &tiles) {
			for j in 1..<i32(CHUNK_HEIGHT+1) {
				for i in 1..<i32(CHUNK_WIDTH+1) {
					chunk.tiles[j-1][i-1] = pick_first(grid[j*(CHUNK_WIDTH+2)+i].superposition)
				}
			}
			return chunk
		}
	}
}

pick_from_slice :: proc(array: []$T) -> T {
	return array[rand.int_max(len(array))]
}

pick_from_bs :: proc(bs: bit_set[$T]) -> T {
	if card(bs) <= 1 {
		return pick_first(bs)
	}
	r := rand.int_max(card(bs))
	k := 0
	for i in bs {
		if k == r {
			return i
		}
		k += 1
	}
	return T(r)
}

pick_first :: proc(bs: bit_set[$T]) -> T {
	for i in bs do return i
	return .GRASS
} 

neighbour_superposition :: proc(c: ^Cell, d: WFC_Direction) -> bit_set[Tile] {
	if (card(c.superposition) < 1) {
		return auto_cast {}
	}
	possible := lookup_table[pick_first(c.superposition)][d]
	for i in c.superposition {
		possible += lookup_table[i][d]
	}
	return possible
}

cell_update :: proc(using wfc: ^WFC, c: ^Cell) -> bool {
	// Collapse possibilities as far as possible
	if c.pos.y > 0 {
		c.superposition &= neighbour_superposition(&grid[(c.pos.y-1)*DIM+c.pos.x], .DOWN)
	}
	if c.pos.y < DIM-1 {
		c.superposition &= neighbour_superposition(&grid[(c.pos.y+1)*DIM+c.pos.x], .UP)
	}
	if c.pos.x > 0 {
		c.superposition &= neighbour_superposition(&grid[c.pos.y*DIM+(c.pos.x-1)], .RIGHT)
	}
	if c.pos.x < DIM-1 {
		c.superposition &= neighbour_superposition(&grid[c.pos.y*DIM+(c.pos.x+1)], .LEFT)
	}
	return card(c.superposition) > 0
}

cell_update_safe :: proc(using wfc: ^WFC, c: ^Cell) -> bool {
	old_superposition := c.superposition
	cell_update(wfc, c)
	if card(c.superposition) < 0 {
		c.superposition = old_superposition
		return false
	}
	return true
}

cell_cmp :: proc(i, j: ^Cell) -> slice.Ordering {
	entropyi := card(i.superposition)
	entropyj := card(j.superposition)

	if entropyi == entropyj {
		return .Equal
	}
	if entropyi > entropyj {
		return .Greater
	}
	return .Less
}

heuristic_picks :: proc(t: ^[]^Cell) -> []^Cell {
	// Take out all collapsed cells from (t^)
	entropy := card((t^)[0].superposition)
	for _, i in (t^) {
		if card((t^)[i].superposition) > entropy {
			entropy = card((t^)[i].superposition)
			(t^) = (t^)[i:]
			// Pick a random cell that has the same entropy
			for _, i in (t^) {
				if card((t^)[i].superposition) > entropy {
					return (t^)[:i-1]
				}
			}
			return (t^)
		}
	}
	picks := (t^)
	(t^) = (t^)[1:]
	return picks
}

wfc_update :: proc(using wfc: ^WFC, t: ^[]^Cell) -> bool {
	slice.sort_by_cmp(t^, cell_cmp)
	picks := heuristic_picks(t)
	if len(t^) < 1 {
		fmt.println(len(t^))
		return true
	}
	last_grid := grid
	next_tiles := (t^)
	using time
	s: Stopwatch
	stopwatch_start(&s)
	update_loop : for {
		if stopwatch_duration(s) > (250 * Millisecond) {
			return false
		}
		// Pick cell with least entropy
		if len(picks) < 1 {
			return false
		}
		cell := picks[0]
		picks = picks[1:]

		// Update the cells superpositions to reflect neighbours
		old_superposition := cell.superposition
		if !cell_update(wfc, cell) {
			cell.superposition = old_superposition
			continue update_loop
		}

		// Collapse into one of its possible states
		cell.superposition = {pick_from_bs(cell.superposition)}
		for _ in 0..<2 do if !wfc_validate(wfc) {
			grid = last_grid
			next_tiles = t^
			continue update_loop
		}
		
		if !wfc_update(wfc, &next_tiles) {
			grid = last_grid
			next_tiles = t^
			continue update_loop
		}
		return true
	}
}

wfc_validate :: proc(using wfc: ^WFC) -> bool {
	for j in 0..<i32(DIM) {
		for i in 0..<i32(DIM) {
			if !cell_update(wfc, &grid[j*DIM+i]) do return false
		}
	}
	return true
}
