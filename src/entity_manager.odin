package main

import "core:time"
import "core:fmt"
import "core:mem"
import rl "raylib"

EntityManager :: struct {
	e:   [^]Entity,
	ids: [^]uint,
	head, tail, amount, allocated, max: uint,
	npc_amount, projectile_amount: uint,
	npc_max, projectile_max: uint
}

em_init :: proc(em: ^EntityManager, capacity := uint(16), max := uint(1024), npc_max := uint(100), projectile_max := uint(100)) {
	ptr, err := mem.alloc(int(capacity*size_of(em.e[0])))
	if err != nil {
		panic("Oh shit")
	}
	em.e = auto_cast ptr
	ptr, err = mem.alloc(int(capacity*size_of(em.ids[0])))
	if err != nil {
		panic("Oh shit")
	}
	em.ids = auto_cast ptr
	for i in 0..<capacity {
		em.ids[i] = i
	}
	em.allocated = capacity
	em.max = max
	em.npc_max = npc_max
	em.projectile_max = projectile_max
}

em_add :: proc(em: ^EntityManager, e: Entity) -> Maybe(uint)
{
	if em.amount >= em.max do return nil
	#partial switch e.type {
		case .TRAINER, .WOLOMON:
			if em.npc_amount >= em.npc_max do return nil
		case .PROJECTILE:
			if em.projectile_amount >= em.projectile_max do return nil
	}
	
	if em.amount >= em.allocated {
		// Double the size of our allocation
		ptr, err := mem.resize(em.ids, int(em.allocated*size_of(em.ids[0])), int(em.allocated*2*size_of(em.ids[0])))
		if err != nil {
			panic("Oh shit")
		}
		em.ids = auto_cast ptr
		ptr, err = mem.resize(em.e, int(em.allocated*size_of(em.e[0])), int(em.allocated*2*size_of(em.e[0])))
		if err != nil {
			panic("Oh shit")
		}
		em.e = auto_cast ptr
		// Copy over ids so the block of ids is contiguous and avoids wrap
		mem.copy(em.ids[em.allocated:], em.ids[:], int(em.tail*size_of(em.ids[0])));

		// Basis of new ids
		id := em.allocated

		// Resize array
		em.tail = em.head + em.allocated
		em.allocated *= 2

		// Generate new ids
		// fill in start of buffer
		for i in 0..<em.head {
			em.ids[i] = id
			id += 1
		}
		// fill in end of buffer
		for i in em.tail..<em.allocated {
			em.ids[i] = id
			id += 1
		}
	}
	// Fill in the new entity with their tag indexing back to where their id is stored
	em.e[em.ids[em.tail]] = e
	em.e[em.ids[em.tail]].tag = em.tail
	id := em.ids[em.tail]

	em.tail += 1
	em.tail %= em.allocated
	em.amount += 1
	#partial switch e.type {
		case .TRAINER, .WOLOMON:
			em.npc_amount += 1
		case .PROJECTILE:
			em.projectile_amount += 1
	}

	return id
}

em_delete :: proc(em: ^EntityManager, id: uint)
{
	#partial switch em.e[id].type {
		case .TRAINER, .WOLOMON:
			em.npc_amount -= 1
		case .PROJECTILE:
			em.projectile_amount -= 1
	}
	// Switchedoodle doo while updating the tag of the swap
	em.ids[em.head], em.ids[em.e[id].tag] = em.ids[em.e[id].tag], em.ids[em.head];
	em.e[em.ids[em.e[id].tag]].tag = em.e[id].tag;
	em.head += 1
	em.head %= em.allocated;
	em.amount -= 1;
}

em_for_each :: proc(em: ^EntityManager, fn: proc(e: ^Entity))
{
	i := em.head
	if em.head == em.tail {
		fn(&em.e[em.ids[i]])
		i += 1
	}
	for {
		if i == em.allocated do i = 0
		if i == em.tail do break
		fn(&em.e[em.ids[i]])
		i += 1
	}
}

em_collision_update :: proc(em: ^EntityManager) {
	i := em.head
	if em.head == em.tail {
		j := i+1
		for {
			if j == em.allocated do j = 0
			if j == em.tail do break
			entity_collision_update(&em.e[em.ids[i]], &em.e[em.ids[j]])
			j += 1
		}
		i += 1
	}
	for {
		if i == em.allocated do i = 0
		if i == em.tail do break
		j := i+1
		for {
			if j == em.allocated do j = 0
			if j == em.tail do break
			entity_collision_update(&em.e[em.ids[i]], &em.e[em.ids[j]])
			j += 1
		}
		i += 1
	}
}
