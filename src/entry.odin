package main

import "core:time"
import "core:fmt"
import "core:math"
import "core:strings"
import "core:mem"
import "core:os"
import "core:slice"
import rl "raylib"
import rand "core:math/rand"
import glm "core:math/linalg/glsl"

Game :: struct {
	start_tick: time.Tick,
	t: f64,
	dt: f32,
}

game: Game
em: EntityManager
player := uint(0)
collision_map: map[glm.ivec2][dynamic]uint
sb := strings.builder_make()
previous_time := game.t

GRID_DIM :: 128
WINDOW_WIDTH  :: 1280
WINDOW_HEIGHT :: 768
MOVESPEED :: 1200

entry :: proc() {
	em_init(&em, 100, 100, 10, 90)
	entity_player := Entity{t = {w = {
		e = {{0, 0}, 0, {0, 0}, {0, 0}, {40, 42}, .PLAYER, .DOWN, .IDLE, time.tick_now(), .TRAINER, 0, false},
		set = {
			specie = .AXQUAL,
			name = Species[.AXQUAL].name,
			level = 5,
			moves = {
				{ move = .FLAMESHOT, usages = 1, max_usage = 1 },
				{ move = .CAPTURE, usages = 1, max_usage = 1 },
				{ move = .BITE, usages = 1, max_usage = 1 },
				{ move = .NULL, usages = 1, max_usage = 1 },
			},
		},
	}}}
	entity_player.hp = auto_cast get_stat_of_set(&entity_player.w.set, .HP)
	player = em_add(&em, entity_player).?
	rl.InitWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Wolomon")
	defer rl.CloseWindow()

	rl.SetTargetFPS(60)
	textures_init()
	world_init(i64(rand.uint64()))
	title_screen()

	game.start_tick = time.tick_now()
	showParty := false;
	for !rl.WindowShouldClose() { // Detect window close button or ESC key
		previous_time = game.t
		game.t = time.duration_seconds(time.tick_since(game.start_tick))
		game.dt = f32(game.t - previous_time)
		new_entity := Entity{w = {
			e = {{em.e[player].x+rand.float32_range(-1, 1)*2048, em.e[player].y+rand.float32_range(-1, 1)*2048}, 0, {0, 0}, {0, 0}, {40, 42}, .NPC, .DOWN, .WALKING, time.tick_now(), .WOLOMON, 0, false},
			set = {
				specie = .AXQUAL,
				name = Species[.AXQUAL].name,
				level = 5,
				moves = {
					{ move = .FLAMESHOT, usages = 1, max_usage = 1 },
					{ move = .FLAMESHOT, usages = 1, max_usage = 1 },
					{ move = .FLAMESHOT, usages = 1, max_usage = 1 },
					{ move = .FLAMESHOT, usages = 1, max_usage = 1 },
				},
			},
		}}
		new_entity.hp = auto_cast get_stat_of_set(&new_entity.w.set, .HP)
		em_add(&em, new_entity)
		em_for_each(&em, entity_ai)
		rl.BeginDrawing()
		defer rl.EndDrawing()
		rl.ClearBackground(rl.BLACK)
		if update() do continue
		render()

		if rl.GuiButton(rl.Rectangle{10, WINDOW_HEIGHT - 40, 120, 30}, "#191#Show Party") {
			showParty = !showParty;
		}

		if showParty {
			result := rl.GuiWindowBox(rl.Rectangle{WINDOW_WIDTH/2 - 200, WINDOW_HEIGHT/2 - 200, 400, 400}, "Party")
			for i in 0..<3 {
				for j in 0..<2 {
					rl.DrawRectangleRounded(rl.Rectangle{WINDOW_WIDTH/2 - 142 + f32(i)*100, WINDOW_HEIGHT/2 - 142 + f32(j)*120, 84, 84}, 0.5, 5, rl.BLUE)
					mon, ok := em.e[player].t.mon[j*3 + i].?
					if !ok do continue
					e := &em.e[mon]
					using anim := animations[e.anim]
					frame := frames[0]
					rl.DrawTexturePro(textures[e.texture],
					rl.Rectangle{f32(frame.x), f32(frame.y + (frame_size.y)*i32(Direction.DOWN)), f32(frame_size.x), f32(frame_size.y)},
					rl.Rectangle{WINDOW_WIDTH/2 - 142 + f32(i)*100 + f32(42-2*frame_size.x), WINDOW_HEIGHT/2 - 142 + f32(j)*120 + f32(42-2*frame_size.y), f32(4*frame_size.x), f32(4*frame_size.y)}, rl.Vector2{0, 0}, 0, rl.WHITE)
					entity_render_bar(e, glm.ivec2{i32(WINDOW_WIDTH/2 - 142 + i*100) + 42-50, i32(WINDOW_HEIGHT/2 - 142 + j*120) + 42-50})
				}
			}
			if result > 0 do showParty = false;
		}

		mouse_pos := rl.GetMousePosition()
		if rl.IsMouseButtonPressed(.LEFT) {
			for i in 0..<u32(4) {
				if mouse_pos.x > 200 + f32(i)*48 && mouse_pos.x < 200 + f32(i)*48 + 32 &&
				   mouse_pos.y > WINDOW_HEIGHT - 50 && mouse_pos.y < WINDOW_HEIGHT + 28 {
					em.e[player].t.current_move = i
				}
			}
		}

		e := &em.e[player].w
		control, ok := em.e[player].t.current_control.?
		if ok {
			control, ok = em.e[player].t.mon[control].?
			e = &em.e[control].w
		}
		for i in 0..<i32(4) {
			rl.DrawTexture(move_icons[e.moves[i].move], 200 + i*48, WINDOW_HEIGHT - 50, rl.WHITE)
		}
		rl.DrawRectangleLines(199 + i32(em.e[player].t.current_move)*48, WINDOW_HEIGHT - 51, 34, 34, rl.WHITE);
	}
}

attack_hitbox: rl.Rectangle

update :: proc() -> bool {
	free_all(context.temp_allocator)
	collision_map = make(map[glm.ivec2][dynamic]uint, allocator = context.temp_allocator)
	em_for_each(&em, entity_update)
	if em.e[player].dead {
		em.e[player].dead = false
		em.e[player].hp = auto_cast get_stat_of_set(&em.e[player].w.set, .HP)
		title_screen()
		game.start_tick = time.tick_now()
		game.t = time.duration_seconds(time.tick_since(game.start_tick))
		previous_time = game.t
		return true
	}
	em_for_each(&em, entity_post_update)
	if em.e[player].anim == .ATTACK {
		#partial switch em.e[player].dir {
			case .DOWN:
			attack_hitbox = rl.Rectangle{em.e[player].pos.x, em.e[player].pos.y + 25, 40, 20}
			case .UP:
			attack_hitbox = rl.Rectangle{em.e[player].pos.x, em.e[player].pos.y - 25, 40, 20}
			case .RIGHT:
			attack_hitbox = rl.Rectangle{em.e[player].pos.x + 25, em.e[player].pos.y, 20, 40}
			case .LEFT:
			attack_hitbox = rl.Rectangle{em.e[player].pos.x - 25, em.e[player].pos.y, 20, 40}
		}
		hitbox_for_each(&attack_hitbox, proc(e: ^Entity) {
			if e.tag == em.e[player].tag do return
			e.hp -= 10
			e.vel += 10*(glm.vec2{attack_hitbox.x, attack_hitbox.y} - em.e[player].pos)
			if e.hp <= 0 do em_delete(&em, em.ids[e.tag])
		})
	}
	return false
}

render :: proc() {
	world_render()
	em_for_each(&em, entity_render)
	using t := &em.e[player].t
}

title_screen :: proc() {
	for !rl.WindowShouldClose() { // Detect window close button or ESC key
	rl.BeginDrawing()
	defer rl.EndDrawing()
	rl.ClearBackground(rl.BLACK)
	world_render()
	rl.DrawRectangle(WINDOW_WIDTH/2 - 90, 190, 180, 70, rl.BLACK)
	title := cstring("Wolomon!")
	rl.DrawText(title, WINDOW_WIDTH/2 - rl.MeasureText(title, 30)/2, 200, 30, rl.WHITE)
	if rl.GuiButton(rl.Rectangle{WINDOW_WIDTH/2 - 60, WINDOW_HEIGHT/2, 120, 30}, "Resume") {
		break
	}
	if rl.GuiButton(rl.Rectangle{WINDOW_WIDTH/2 - 60, WINDOW_HEIGHT/2 + 40, 120, 30}, "New Game") {
		os.remove_directory("save/chunks")
		os.make_directory("save/chunks")
		break
	}
	if rl.GuiButton(rl.Rectangle{WINDOW_WIDTH/2 - 60, WINDOW_HEIGHT/2 + 80, 120, 30}, "Quit") {
		for coord in world {
			chunk := &world[coord]
			if chunk.is_changed == false do continue
			strings.write_string(&sb, "save/chunks/")
			strings.write_int(&sb, int(coord.x));
			strings.write_byte(&sb, '-')
			strings.write_int(&sb, int(coord.y));
			os.write_entire_file(strings.to_string(sb), slice.to_bytes(chunk.tiles[:]))
			strings.builder_reset(&sb)
		}
		os.exit(0)
	}
}
}
