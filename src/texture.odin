package main

import "core:fmt"
import "core:time"
import "core:strings"
import math "core:math"
import glm "core:math/linalg/glsl"
import rl "raylib"

Texture :: enum {
	OVERWORLD,
	PLAYER,
	NPC,
	FIREBALL,
}

texture_names: [Texture]cstring = {
	.OVERWORLD = "assets/gfx/Overworld.png",
	.PLAYER  = "assets/gfx/character.png",
	.NPC  = "assets/gfx/NPC_test.png",
	.FIREBALL = "assets/gfx/fireball.png",
}

move_icons: [MoveIndex]rl.Texture2D

textures: [Texture]rl.Texture2D

textures_init :: proc() {
	for i in Texture {
		textures[i] = rl.LoadTexture(texture_names[i]);
	}
	path := "assets/move_icons/"
	ext := ".png"
	for i in MoveIndex {
		name := Moves[i].name
		str := make([^]u8, len(path)+len(name)+len(ext), context.temp_allocator)
		for j in 0..<len(path) {
			str[j] = path[j]
		}
		for j in 0..<len(name) {
			if name[j] >= 'A' && name[j] <= 'Z' do str[j+len(path)] = name[j] + 32
			else if name[j] == ' ' do str[j+len(path)] = '_'
			else do str[j+len(path)] = name[j]
		}
		copy(str[len(path)+len(name):len(path)+len(name)+len(ext)], ext)
		move_icons[i] = rl.LoadTexture(cstring(str))
	}
}
